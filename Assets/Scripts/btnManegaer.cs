﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class btnManegaer : MonoBehaviour
{
    private GUIStyle guiStyle = new GUIStyle();
    private GUIStyle customButton = new GUIStyle();
    public Texture btnTexture;
    private bool next;
    public string stringToEdit;
    public Button newGame;
    public Button rank;
    public Button exit;
    public Texture2D[] buttonTexture;
    public Texture2D TextTexture;
    public void NewGameBtn()
    {
        next = true;
        newGame.gameObject.SetActive(false);
        rank.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);
    }
    private void OnGUI()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
        if (next == true)
        {
            stringToEdit = GUI.TextField(new Rect(Screen.width * 38 / 100, Screen.height * 53 / 100 + 10, 200, 30), stringToEdit, 30);
            guiStyle.fontSize = 29;
            guiStyle.normal.textColor = Color.cyan;
            GUI.Label(new Rect(310, 300, 180, 140), TextTexture);
            customButton.normal.textColor = Color.cyan;
            customButton.fontSize = 15;
            if ((stringToEdit.Length > 1)&&(stringToEdit.Length<15))
            {
                GUILayout.Space(290);
                if (GUI.Button(new Rect(Screen.width * 38 / 100, Screen.height * 62 / 100 + 10, 200, 60), buttonTexture[3]))
                {
                    PlayerPrefs.SetString("name", stringToEdit);
                    SceneManager.LoadScene("main");
                }
            }
        }
    }

    public void NewRankBtn()
    {
        SceneManager.LoadScene("Ranking");
    }
    public void exitBtn()
    {
        PlayerPrefs.Save();
        Application.Quit();
    }
}
