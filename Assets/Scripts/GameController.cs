﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    private AudioSource[] pausedSources;

    public bool startGame = false;
    public int score;
    public bool gameOver;
    public bool restart;
    public bool pause;

    AudioSource thrusterSource;
    AudioSource guardSource;

    void Start()
    {
        AudioSource[] AudioClips = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine(SpawnWaves());
        AudioClips[0].Play();

    }

    private void Update()
    {
     
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("Menu");
            }
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (pause)
            {
                Time.timeScale = 1;
                gameOverText.text = "";
                pause = false;
                foreach (AudioSource source in pausedSources)
                {
                    if (source)
                    {
                        source.Play();
                    }
                }
            }
            else
            {   // Pauza 

                Time.timeScale = 0;
                gameOverText.text = "Pause";
                pause = true;
                // Wyciszanie dźwięku 
                AudioSource[] aSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
                pausedSources = new AudioSource[aSources.Length];
                for (int i = 0; i < aSources.Length; i++)
                {
                    AudioSource source = aSources[i];
                    if (source.isPlaying)
                    {
                        source.Pause();
                        pausedSources[i] = source;
                    }
                }
            }
        }
    }
    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                if (gameOver) { break; }
                else
                {
                
                    GameObject hazard = hazards[Random.Range(0,hazards.Length)];
                    Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                    Quaternion spawnRotation = Quaternion.identity;
                    Instantiate(hazard, spawnPosition, spawnRotation);
                    yield return new WaitForSeconds(spawnWait);
                }
            }
            yield return new WaitForSeconds(waveWait);
            if (gameOver)
            {
                AudioSource[] AudioClips = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
                AudioClips[0].Stop();
                AudioClips[1].Play();
                restartText.text = "Press 'R' to Restart";
                PlayerPrefs.SetFloat("score", score);
                HighScoreManager._instance.SaveHighScore(PlayerPrefs.GetString("name"), (int)PlayerPrefs.GetFloat("score"));
                restart = true;
                break;
            }

        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        gameOverText.text = "GAME OVER :(";
        gameOver = true;
    }
}
