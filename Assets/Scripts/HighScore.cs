﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HighScore : MonoBehaviour
{
    private bool gui;
    new string name = "";
    new string score = "";
    private GUIStyle guiStyle = new GUIStyle();
    public Texture2D[] Textexture;

    List<Scores> highscore;

    void Start()
    {
        gui = true;
        highscore = new List<Scores>();
        highscore = HighScoreManager._instance.GetHighScore();
    }
    private void OnGUI()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
           HighScoreManager._instance.ClearLeaderBoard();
            SceneManager.LoadScene("Ranking");
        }

        GUILayout.Space(250);
        guiStyle.fontSize = 27;
        guiStyle.normal.textColor = Color.red;
        GUI.Label(new Rect(150, 230, 200, 150), Textexture[0]);
        guiStyle.normal.textColor = Color.black;
        GUI.Label(new Rect(470, 230, 120, 100), Textexture[1]);
        GUILayout.Space(50);

        foreach (Scores _score in highscore)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("                 -> " + _score.name, guiStyle, GUILayout.Width((Screen.width / 2) + 125));
            GUILayout.Label("" + _score.score, guiStyle, GUILayout.Width((Screen.width / 2) + 125));
            GUILayout.EndHorizontal();
        }
    }
}
